class PrincipalDAO {
    conn = new TourDB();
    async insert(Table, obj) {
        const result = await new Promise(async (result, error) => {
            const query = await this.conn.getObjectStore(Table, READ_WRITE);
            let guardo;
            try {
                guardo = query.add(obj);
            } catch (e) {
                error(e);
            }
            guardo.onsuccess = (s) => result(true);
            guardo.onerror = (e) => error(e);
        });

        return result;
    }
    async update(table, objeto) {
        const result = await new Promise(async (result, error) => {
            const query = await this.conn.getObjectStore(table, READ_WRITE);

            let guardo;
            try {
                guardo = query.put(objeto);
            } catch (e) {
                error(e);
            }
            guardo.onsuccess = (s) => result(true);
            guardo.onerror = (e) => error(e);


        });
        return result;
    }
    async delete(Table, id) {
        const result = await new Promise(async (resolve, reject) => {
            const query = await this.conn.getObjectStore(Table, READ_WRITE);
            const valor = query.get(id);
            valor.onsuccess = (e) => {
                let resp = e.target.result;
                if (typeof resp === undefined)
                    resolve(false);
                resp = query.delete(id);
                resp.onsuccess = (e) => resolve(true);
                resp.onerror = (e) => reject(e);
            }

        });
        return result;
    }
    async getById(Table, id) {
        const result = await new Promise(async (result, error) => {
            const query = await this.conn.getObjectStore(Table, READ_ONLY);
            const valor = query.get(id);
            valor.onsuccess = (e) => result(e.target.result);
            valor.onerror = (e) => error(e);
        });
        return result;
    }
    async getAll(Table) {
        const result = await new Promise(async (result, error) => {
            const query = await this.conn.getObjectStore(Table, READ_ONLY)
            const valor = query.getAll();
            valor.onsuccess = (r) => result(r.target.result);
            valor.onerror = (e) => error(e);
        });
        return result;
    }
    async deleteAll(Table) {
        const result = await new Promise(async (result, error) => {
            const query = await this.conn.getObjectStore(Table, READ_WRITE);
            query.clear();
            result(true);
        });
        return result;
    }

    async getAllByUserType(usertype) {
        const result = await new Promise(async (result, error) => {
            const query = await this.getObjectStore(USUARIOS, READ_ONLY);
            let index = query.index('usertype');
            let cursorRe = index.openCursor();
            const data = [];
            cursorRe.onsuccess = (e) => {
                let cursor = e.target.result;
                if (cursor) {
                    if (cursor.key === usertype)
                        data.push(cursor.value);
                    cursor.continue();
                } else
                    result(data);
            };

        });
        return result;
    }

}
