const DB_NAME = 'ToursDB';
const DB_VERSION = 3;
const USUARIOS = 'obj_usuarios';
const RESERVA = 'obj_reserva';
const READ_WRITE = 'readwrite';
const READ_ONLY = 'readonly';
var dbConn;

class TourDB {
    open() {
        return new Promise((result, error) => {
            // const request = indexedDB.deleteDatabase(DB_NAME);
            const request = indexedDB.open(DB_NAME, DB_VERSION);
            request.onupgradeneeded = (e) => {
                dbConn = request.result;
                if (!dbConn.objectStoreNames.contains(USUARIOS)) {
                    const compo = dbConn.createObjectStore(USUARIOS, { keyPath: 'id' });
                    compo.createIndex('objeto', 'objeto', { unique: false });
                    compo.createIndex('usertype', 'usertype', { unique: false });
                }
                if (!dbConn.objectStoreNames.contains(RESERVA)) {
                    const compo = dbConn.createObjectStore(RESERVA, { keyPath: 'id' });
                    compo.createIndex('objeto', 'objeto', { unique: false });
                }
            }
            request.onsuccess = (e) => {
                dbConn = request.result;
                result(request.result);
            }

            request.onerror = (e) => {

                error(e);
            }
        });
    }
    getObjectStore(table, modo) {
        return new Promise(async (result, error) => {
            let ts;
            const conn = await this.open();
            ts = dbConn.transaction(table, modo);
            result(ts.objectStore(table));
        });
    }
    dropDatabase() {
        return new Promise((resolve, reject) => {
            const request = indexedDB.deleteDatabase(DB_NAME);
            request.onsuccess = (e) => resolve(e);
            request.onerror = (error) => reject(error);
        });
    }
}
